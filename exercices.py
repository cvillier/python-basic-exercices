"""
Exercices.

Implémentez les méthodes ci-dessous.
Pour executer vos tests il vous faudra utiliser pytest
"""

def htc_to_ttc(htc_cost: float, discount_rate: float = 0) -> float :
    """
    Exercice 1 :
    Calcule le coût TTC d'un produit.
    discount_rate : taux de réduction compris entre 0 et 1
    Taux de taxes : 20.6 %
    Retourne un float arrondi à deux décimales
    """
    result = round(htc_cost * (1 - discount_rate) * 1.206, 2)

    return result

print(htc_to_ttc(200, 0.5))


def divisors(value: int = 0):
    """
    Exercice 2 :
    A partir d'un nombre donné,
    retourne ses diviseurs (sans répétition)
    s’il y en a, ou « PREMIER » s’il n’y en a pas.
    """
    result = []

    i = value
    while i != 0:
        if value%i == 0:
            result.append(i)
        i -= 1

    if len(result) == 2:
        result = 'PREMIER ! (comme François ;) )'

    return result

print(divisors(31))
